import { TestBed, ComponentFixture } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { FormsModule } from '@angular/forms';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatIconModule } from '@angular/material/icon';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { of } from 'rxjs';
import { StargateService } from './stargate.service';
import { Astronaut } from './astronaut.model';

describe('AppComponent', () => {
  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;
  let stargateServiceSpy: jasmine.SpyObj<StargateService>;

  beforeEach(async () => {
    const spy = jasmine.createSpyObj('StargateService', [
      'getAstronautDetailsByName',
    ]);

    await TestBed.configureTestingModule({
      imports: [
        BrowserAnimationsModule,
        HttpClientModule,
        MatCardModule,
        MatInputModule,
        MatFormFieldModule,
        MatButtonModule,
        MatIconModule,
        FormsModule,
        MatSnackBarModule,
        MatProgressSpinnerModule,
      ],
      providers: [{ provide: StargateService, useValue: spy }],
    }).compileComponents();

    stargateServiceSpy = TestBed.inject(
      StargateService
    ) as jasmine.SpyObj<StargateService>;
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create the app component', () => {
    expect(component).toBeTruthy();
  });


  it('should display astronaut details when search is successful', () => {
    const expectedAstronaut: Astronaut = {
      personId: 1,
      name: 'Test Astronaut',
      currentRank: 'I',
      currentDutyTitle: 'Test Duty',
      careerStartDate: new Date('2022-01-01'),
      careerEndDate: null,
    };

    component.searchQuery = 'TestAstronaut';
    stargateServiceSpy.getAstronautDetailsByName.and.returnValue(
      of(expectedAstronaut)
    );

    component.search();

    expect(component.astronautDetails).toEqual(expectedAstronaut);
    expect(component.loading).toBeFalse();
  });
});
