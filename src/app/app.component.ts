import { Component } from '@angular/core';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatIconModule } from '@angular/material/icon';
import { MatSnackBar } from '@angular/material/snack-bar';
import { StargateService } from './stargate.service';
import { Astronaut } from './astronaut.model';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [
    HttpClientModule,
    CommonModule,
    MatCardModule,
    MatInputModule,
    MatFormFieldModule,
    MatButtonModule,
    MatIconModule,
    FormsModule,
    MatSnackBarModule,
    MatProgressSpinnerModule
  ],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css',
})
export class AppComponent {
  searchQuery: string = '';
  astronautDetails: Astronaut | null = null;
  loading: boolean = false;

  constructor(
    private stargateService: StargateService,
    private snackBar: MatSnackBar
  ) {}

  search() {
    this.loading = true;
    this.astronautDetails = null;

    this.stargateService.getAstronautDetailsByName(this.searchQuery).subscribe({
      next: (data) => {
        this.astronautDetails = data;
        this.loading = false;
      },
      error: (error) => {
        this.snackBar.open('Astronaut not found!', 'Close', {
          duration: 3000,
          verticalPosition: 'top',
        });
        this.loading = false;
      },
    });
  }
}
