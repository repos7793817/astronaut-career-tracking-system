import { TestBed } from '@angular/core/testing';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { StargateService } from './stargate.service';
import { Astronaut } from './astronaut.model';
import { AppConfig } from '../../environment';

describe('StargateService', () => {
  let service: StargateService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [StargateService],
    });
    service = TestBed.inject(StargateService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should retrieve astronaut details by name', () => {
    const expectedAstronaut: Astronaut = {
      personId: 1,
      name: 'Marc',
      currentRank: 'I',
      currentDutyTitle: 'Test',
      careerStartDate: new Date('2024-05-09'),
      careerEndDate: null,
    };

    const searchName = 'Marc';

    service.getAstronautDetailsByName(searchName).subscribe((astronaut) => {
      expect(astronaut).toEqual(expectedAstronaut);
    });

    const req = httpMock.expectOne(
      AppConfig.apiUrl + '/AstronautDuty/Marc'
    ); // Expect a single HTTP request with the specified URL
    expect(req.request.method).toBe('GET'); // Assert that the request method is GET

    req.flush({ person: expectedAstronaut }); // Simulate a response with the expected astronaut data
  });

  it('should map response to Astronaut object', () => {
    const mockResponse = {
      person: {
        personId: 1,
        name: 'Test Astronaut',
        currentRank: 'I',
        currentDutyTitle: 'Test Duty',
        careerStartDate: '2022-01-01',
        careerEndDate: null,
      },
    };

    const expectedAstronaut: Astronaut = {
      personId: 1,
      name: 'Test Astronaut',
      currentRank: 'I',
      currentDutyTitle: 'Test Duty',
      careerStartDate: new Date('2022-01-01'),
      careerEndDate: null,
    };

    const mappedAstronaut = service['mapResponseToAstronaut'](mockResponse);
    expect(mappedAstronaut).toEqual(expectedAstronaut);
  });
});
