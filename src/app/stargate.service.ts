import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, Observable } from 'rxjs';
import { Astronaut } from './astronaut.model';
import { AppConfig } from '../../environment';


@Injectable({
  providedIn: 'root',
})
export class StargateService {
  protected apiUrl = AppConfig.apiUrl;

  constructor(private http: HttpClient) {}

  getAstronautDetailsByName(name: string): Observable<Astronaut> {
    return this.http
      .get<any>(`${this.apiUrl}/AstronautDuty/${name}`)
      .pipe(map((response) => this.mapResponseToAstronaut(response)));
  }

  private mapResponseToAstronaut(response: any): Astronaut {
    return {
      personId: response.person.personId,
      name: response.person.name,
      currentRank: response.person.currentRank,
      currentDutyTitle: response.person.currentDutyTitle,
      careerStartDate: new Date(response.person.careerStartDate),
      careerEndDate: response.person.careerEndDate ? new Date(response.person.careerEndDate) : null,
    };
  }
}
